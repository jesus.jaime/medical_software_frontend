import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../interceptors/auth.interceptor';


import { CoreModule } from '../modules/core-module.module';
import { ScheduleStatusModule } from '../modules/components/schedule-status.module';
import { ScheduleEventModule } from '../modules/components/schedule-event.module';
import { UserModule } from '../modules/components/user.module';
import { DeviceService } from '../services/device.service';
import { RoleModule } from '../modules/components/role.module';








@NgModule({
  declarations: [


  ],
  imports: [
    CoreModule,
    ScheduleStatusModule,
    ScheduleEventModule,
    UserModule,
    RoleModule

  ],
  providers: [
    DeviceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
