import { TestBed, async, } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from '../components/_partials/main-nav/main-nav.component';
import { MatSidenav, MatToolbar, MatNavList, MatIcon, MatSidenavContent, MatSidenavContainer } from '@angular/material';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MainNavComponent,
        MatSidenav,
        MatToolbar,
        MatNavList,
        MatIcon,
        MatSidenavContent,
        MatSidenavContainer
      ],
      imports: [RouterTestingModule, BrowserModule, BrowserAnimationsModule]
    }).compileComponents();
  }));


  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('MEDICAL MARKET PLACES');
  }));
});
