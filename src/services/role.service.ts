import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Role } from '../classes/role';
import { Observable } from 'rxjs';

@Injectable()
export class RoleService {
  constructor(private http: HttpClient) {

  }

  saveRole(role: Role[]) {
    return this.http.post('roles/save', role);
  }

  updateRole(roleId: number, role: Role[]) {
    return this.http.post(`roles/update/${roleId}`, role);
  }

  fetchByPage(filter = '', sortOrder, pageSize = 3, pageIndex): Observable<any> {

    return this.http.post<{}>('roles/fetchByPage', {
      filter,
      sortOrder,
      pageSize
    },
      {
        params: new HttpParams()
          .set('page', (pageIndex + 1).toString())
      }
    );

  }

  isCodeAvailable(code: string): Observable<any> {
    return this.http.get<any>(`roles/isCodeAvailable/${code}`);

  }


  findAvailableRoles(): Observable<Role[]> {
    return this.http.get<Role[]>('roles/findAvailable');
  }
}
