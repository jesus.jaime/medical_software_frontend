import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../classes/user';
import { AbstractControl } from '@angular/forms';
import { Role } from '../classes/role';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {

  }

  isEmailAvailable(email: string): Observable<any> {
    return this.http.get<any>(`users/isEmailAvailable/${email}`);
  }



  // Fix issue with interface ***
  findUsers(filter = '', sortOrder, pageSize = 3, pageIndex): Observable<any> {

    return this.http.post<{}>('users/fetch', {
      filter,
      sortOrder,
      pageSize
    },
      {
        params: new HttpParams()
          .set('page', (pageIndex + 1).toString())
      }
    );
  }

  saveUser(user: User) {
    return this.http.post('users/save', user);
  }

  updateUser(userId: number, user: User) {

    return this.http.post(`users/update/${userId}`, user);
  }

}
