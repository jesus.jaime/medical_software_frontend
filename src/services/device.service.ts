import { Injectable } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogSizes } from 'src/classes/dialog-sizes';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DeviceService {

  public windowResized = new BehaviorSubject<boolean>(false);

  public breakpoints = {
    'xs': 575.98,
    'sm': 767.98,
    'md': 991.98,
    'lg': 1199.98
  };



  constructor() {
  }



  updateDialogSize(windowWidth: number, dialogSizes: DialogSizes, dialogRef: MatDialogRef<any>) {


    //Mobile Device Dialog Size
    if (windowWidth < this.breakpoints.sm) {
      dialogRef.updateSize(dialogSizes.sm.innerWidth, dialogSizes.sm.innerHeight);
    }

    //Tablet Device Dialog Size
    if (windowWidth > this.breakpoints.sm && windowWidth < this.breakpoints.md) {
      dialogRef.updateSize(dialogSizes.md.innerWidth, dialogSizes.md.innerHeight);

    }
    //Desktop  Dialog Size
    if (windowWidth > this.breakpoints.md) {
      dialogRef.updateSize(dialogSizes.lg.innerWidth, dialogSizes.lg.innerHeight);

    }

  }



}
