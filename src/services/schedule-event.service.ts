import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ScheduleEvent } from '../classes/schedule-event';
import { Observable } from 'rxjs';
import { PaginatorFetcher } from 'src/classes/paginator-fetcher';



@Injectable()
export class ScheduleEventService {

  constructor(private http: HttpClient) {

  }


  // Fix issue with interface ***
  findEventTypes(paginatorFetcher: PaginatorFetcher): Observable<any> {

    return this.http.post<{}>('scheduleEventsType/fetch', {
      ...paginatorFetcher
    },
      {
        params: new HttpParams()
          .set('page', (paginatorFetcher.pageIndex + 1).toString())
      }
    );
  }

  saveEventType(scheduleEvent: ScheduleEvent) {
    return this.http.post('scheduleEventsType/save', scheduleEvent);
  }

  updateEventType(scheduleEventId: number, scheduleEvent: ScheduleEvent) {

    return this.http.post(`scheduleEventsType/update/${scheduleEventId}`, scheduleEvent);
  }

  setAvailability(scheduleEventId: number, status: number) {
    return this.http.get(`scheduleEventsType/setAvailability/${scheduleEventId}/${status}`)
  }


}
