import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ScheduleStatus } from '../classes/schedule-status';
import { Observable } from 'rxjs';



@Injectable()
export class ScheduleStatusService {

  constructor(private http: HttpClient) {

  }


  // Fix issue with interface ***
  findStatuses(filter = '', sortOrder, pageSize = 3, pageIndex): Observable<any> {

    return this.http.post<{}>('scheduleStatusType/fetch', {
      filter,
      sortOrder,
      pageSize
    },
      {
        params: new HttpParams()
          .set('page', (pageIndex + 1).toString())
      }
    );
  }

  saveStatus(scheduleStatus: ScheduleStatus) {
    return this.http.post('scheduleStatusType/save', scheduleStatus);
  }

  updateStatus(scheduleStatusId: number, scheduleStatus: ScheduleStatus) {

    return this.http.post(`scheduleStatusType/update/${scheduleStatusId}`,  scheduleStatus );
  }


}
