export interface DialogSizes {


    sm: {
        innerWidth: string,
        innerHeight: string
    },
    md: {
        innerWidth: string,
        innerHeight: string
    },
    lg: {
        innerWidth: string,
        innerHeight: string
    }

}