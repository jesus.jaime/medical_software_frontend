export class Address {
  id: number;
  street: string;
  ext_number: string;
  int_number: string;
  settlement: string;
  city_council: string;
  estate: string;

}
