import { Contact } from "./contact";

export class Person {
  id: number;
  name: string;
  first_name: string;
  last_name: string;
  contacts: Contact[];

}
