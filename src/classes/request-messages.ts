export class RequestMessages {

  public static readonly REQUESTS: string[] = [
    '/scheduleStatusType/save',
    '/scheduleStatusType/update',
    '/scheduleEventsType/save',
    '/scheduleEventsType/update'
  ];

}
