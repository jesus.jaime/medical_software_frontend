export interface PacientPermissions {
    can_create: number;
    can_update: number;
    can_deactivate: number;
    can_see_reports: number;
    can_see_schedule: number;
    is_pacient_owner: number;
}