import { PacientPermissions } from "./pacient-permissions";
import { OfficePermissions } from "./office-permissions";
import { UserPermissions } from "./user-permissions";

export interface Role {
  id: number;
  code: string;
  description: string;
  office_permissions: OfficePermissions;
  pacient_permissions: PacientPermissions;
  user_permissions: UserPermissions;
  created_at: string;
  updated_at: string;
  is_active: number;
}


