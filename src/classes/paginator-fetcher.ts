export interface PaginatorFetcher {

    queryString: string;
    sortDirection: string;
    pageSize: number;
    pageIndex: number;

}