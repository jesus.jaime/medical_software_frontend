export interface ScheduleEvent {
  id: number;
  name: string;
  color: string;
  created_at: string;
  updated_at: string;
  is_active: number;
}

