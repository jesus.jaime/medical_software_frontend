import { Person } from './person';
import { Role } from './role';

export interface User {
  id: number;
  person: Person;
  roles: Role[];
  email: string;
  password?: string;
  is_account_owner: number;
  is_active: number;
  created_at: string;
  updated_at: string;


}
