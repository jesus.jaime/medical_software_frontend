export interface UserPermissions {
    can_create: number;
    can_update: number;
    can_deactivate: number;
}