export interface OfficePermissions {
    can_create: number;
    can_update: number;
    can_deactivate: number;
}