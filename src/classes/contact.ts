export interface Contact {
  id: number;
  type: string;
  contact: string;
  created_at: string;
  updated_at: string;
}
