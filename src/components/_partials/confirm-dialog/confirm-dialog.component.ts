import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  confirmData: { title: string, description: string } = {
    title: '',
    description: ''
  };


  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: { title; description; },
  ) {


  }

  ngOnInit() {

    this.confirmData.title = this.data.title;
    this.confirmData.description = this.data.description;
  }

  onConfirm(answer: boolean) {
    this.dialogRef.close({
      answer
    });
  }

}
