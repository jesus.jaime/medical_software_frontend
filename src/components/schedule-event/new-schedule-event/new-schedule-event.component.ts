import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ScheduleEventComponent } from '../schedule-event.component';
import { ScheduleEvent } from '../../../classes/schedule-event';
import { DialogSizes } from 'src/classes/dialog-sizes';
import { Subscription, fromEvent } from 'rxjs';
import { DeviceService } from 'src/services/device.service';

@Component({
  selector: 'app-new-schedule-event',
  templateUrl: './new-schedule-event.component.html',
  styleUrls: ['./new-schedule-event.component.scss']
})
export class NewScheduleEventComponent implements OnInit, OnDestroy {

  dialogTitle: String = '';
  statusColor: String = '';
  scheduleEvForm: FormGroup;
  dialogSizes: DialogSizes = {
    'sm': {
      'innerWidth': '90%',
      'innerHeight': '50%'
    },
    'md': {
      'innerWidth': '70%',
      'innerHeight': '40%'
    },
    'lg': {
      'innerWidth': '30%',
      'innerHeight': '45%'
    }

  };
  windowResizeSub: Subscription;

  constructor(
    public dialogRef: MatDialogRef<ScheduleEventComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ScheduleEvent,
    private formBuilder: FormBuilder,
    private deviceService: DeviceService
  ) {

    this.statusColor = '#eaeaea';
    this.initForm();


  }


  /* It receives a schedule event object from an opened Dialog (Create or update), set the values to the form.
    Size default dialog size
    Resize dialog sizw on window resize event.   
  */
  ngOnInit(): void {
    this.dialogTitle = !this.data.id ? 'Nuevo Evento' : 'Editar Evento';

    if (this.data.id) {
      this.fillForm();
    }

    this.deviceService.updateDialogSize(window.innerWidth, this.dialogSizes, this.dialogRef);

    /* this.windowResizeSub = fromEvent(window, 'resize').subscribe((event: any) => {
      this.deviceService.updateDialogSize(event.target.innerWidth, this.dialogSizes, this.dialogRef);
    });
 */
  }

  ngOnDestroy(): void {
    //this.windowResizeSub.unsubscribe();

  }

  initForm(): void {
    this.scheduleEvForm = this.formBuilder.group({
      'name': [
        null,
        Validators.required],
      'color': [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/^#[0-9A-F]{6}$/i)
        ])
      ]
    });

  }

  fillForm(): void {
    this.statusColor = this.data.color;
    this.scheduleEvForm.get('name').setValue(this.data.name);
    this.scheduleEvForm.get('color').setValue(this.data.color);
  }

  /* Set a value to the color form control */
  onSelectColor(event) {
    this.scheduleEvForm.get('color').setValue(this.statusColor);
  }

  /* Return a scheduleEventType object to Schedule Event Component to do a http request */
  onSubmit() {

    this.dialogRef.close({
      id: this.data == null ? null : this.data.id,
      scheduleEvent: this.scheduleEvForm.value
    });

  }

}
