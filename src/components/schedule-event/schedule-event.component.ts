import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';

import { merge, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, map } from 'rxjs/operators';

import { ScheduleEvent } from '../../classes/schedule-event';

import { ScheduleEventDataSource } from './schedule-event-datasource';
import { NewScheduleEventComponent } from './new-schedule-event/new-schedule-event.component';
import { ScheduleEventService } from '../../services/schedule-event.service';
import { PaginatorFetcher } from 'src/classes/paginator-fetcher';
import { ConfirmDialogComponent } from '../_partials/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'app-schedule-event',
  templateUrl: './schedule-event.component.html',
  styleUrls: ['./schedule-event.component.scss']
})
export class ScheduleEventComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  searchInput: FormControl;
  dataSource: ScheduleEventDataSource;
  basePaginatorFetcher: PaginatorFetcher = {
    queryString: '',
    sortDirection: 'asc',
    pageSize: 10,
    pageIndex: 0
  };;




  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['color', 'name', 'created_at', 'updated_at', 'is_active', 'actions'];


  constructor(
    private scheduleEvService: ScheduleEventService,
    public matDialog: MatDialog) {
    this.searchInput = new FormControl();

  }

  /* Initialize datasource and subscribe an fetch first events page */
  ngOnInit(): void {
    this.dataSource = new ScheduleEventDataSource(this.scheduleEvService);
    this.dataSource.fetchEventTypes(this.basePaginatorFetcher);
    this.paginator._intl.itemsPerPageLabel = 'Filas por página';

  }


  ngAfterViewInit(): void {

    // server-side input search
    this.searchInput.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.dataSource.loadingSubject.next(true);
        this.dataSource.fetchEventTypes(
          {
            queryString: this.searchInput.value,
            sortDirection: this.sort.direction,
            pageSize: this.paginator.pageSize,
            pageIndex: this.paginator.pageIndex
          }
        );


      })
    ).subscribe();


    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {

          this.dataSource.fetchEventTypes(
            {
              queryString: this.searchInput.value,
              sortDirection: this.sort.direction,
              pageSize: this.paginator.pageSize,
              pageIndex: this.paginator.pageIndex
            }
          )
            ;
        })
      ).subscribe();

  }



  /* Open a dialog to create or update a calendar event type via POST */
  openDialog(scheduleEvent?: ScheduleEvent) {

    const dialogRef = this.matDialog.open(NewScheduleEventComponent, {
      panelClass: 'custom-dialog',
      data: {
        ...scheduleEvent
      }
    });


    dialogRef.afterClosed().subscribe(result => {


      /*POST A NEW EVENT TYPE */
      if (result && result.id == null) {

        this.dataSource.loadingSubject.next(true);
        this.dataSource.eventsTypeData.next([]);
        this.scheduleEvService.saveEventType(result.scheduleEvent)
          .pipe(
            map(() => {
              this.dataSource.fetchEventTypes(this.basePaginatorFetcher);
            })
          )
          .subscribe();
      }

      /*UPDATE EVENT TYPE */
      if (result && result.id != null) {

        this.dataSource.loadingSubject.next(true);
        this.dataSource.eventsTypeData.next([]);
        this.scheduleEvService.updateEventType(result.id, result.scheduleEvent)
          .pipe(
            map(() => {
              this.dataSource.fetchEventTypes(this.basePaginatorFetcher);
            })
          )
          .subscribe();


      }
    });

  }

  setEventAvailability(scheduleEvent: ScheduleEvent) {

    const status = scheduleEvent.is_active ? 'desactivar' : 'activar';
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      panelClass: 'custom-dialog',
      width: '400px',
      height: '200px',
      data: {
        title: 'Tipos de Evento de Agenda',
        description: `¿Realmente desea ${status} el evento ${scheduleEvent.name} ?`
      }
    });


    dialogRef.afterClosed().subscribe(result => {

      if (result.answer != undefined && result.answer) {
        this.dataSource.loadingSubject.next(true);
        this.dataSource.eventsTypeData.next([]);

        const status = scheduleEvent.is_active ? 0 : 1;
        this.scheduleEvService.setAvailability(scheduleEvent.id, status).subscribe(() => {
          this.dataSource.fetchEventTypes(this.basePaginatorFetcher);
        });
      }


    });
  }
}
