import { DataSource, CollectionViewer } from '@angular/cdk/collections';


import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';
import { map, catchError, finalize, first } from 'rxjs/operators';

import { ScheduleEvent } from 'src/classes/schedule-event';
import { ScheduleEventService } from 'src/services/schedule-event.service';
import { OnInit } from '@angular/core';
import { PaginatorFetcher } from 'src/classes/paginator-fetcher';


export class ScheduleEventDataSource extends DataSource<ScheduleEvent> {


  public eventsTypeData = new BehaviorSubject<ScheduleEvent[]>([]);
  public loadingSubject = new BehaviorSubject<boolean>(true);
  private pagesSubject = new BehaviorSubject<number>(1);

  public loading$ = this.loadingSubject.asObservable();
  public noPages$ = this.pagesSubject.asObservable();

  constructor(private scheduleEvService: ScheduleEventService) {
    super();
  }


  /*
    Called when data table bootstrap itself.
    It emits a Schedule Event Type list whenever the user trigger a paginator event
    It implements a subject, with this the subscribers can get the latest emitted value
  */
  connect(collectionViewer: CollectionViewer): Observable<ScheduleEvent[]> {
    return this.eventsTypeData.asObservable();
  }


  /*
    Called when data table gets destroyed.
    It avoids memory leaks when the observables get completed
  */
  disconnect(collectionViewer: CollectionViewer): void {
    this.eventsTypeData.complete();
    this.loadingSubject.complete();
  }



  /*
    It Emmits a eventTypesData whenever the user triggers a paginator event (empty in case of error).
    Show loading spinner before it do a request, when the request is completed the loading spinner gets hidden
  */
  fetchEventTypes(paginatorFetcher: PaginatorFetcher) {

    this.eventsTypeData.next([]);
    this.scheduleEvService.findEventTypes(paginatorFetcher).pipe(

      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))

    ).subscribe(pagination => {

      if (pagination.data.length > 0)
        this.eventsTypeData.next(<ScheduleEvent[]>pagination.data);
      else
        this.eventsTypeData.next(null);

        
      this.pagesSubject.next(pagination.total);

    });

  }


}
