import { DataSource, CollectionViewer } from '@angular/cdk/collections';

import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';

import { UserService } from '../../services/user.service';
import { User } from '../../classes/user';


export class UserDataSource extends DataSource<User> {

  private usersData = new BehaviorSubject<User[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private pagesSubject = new BehaviorSubject<number>(1);

  public loading$ = this.loadingSubject.asObservable();
  public noPages$ = this.pagesSubject.asObservable();

  constructor(private userService: UserService) {
    super();
  }

  /*
    Called when data table bootstrap itself.
    It emits a users list whenever the user trigger a paginator event
    It implements a subject, with this the subscribers can get the latest emitted value
  */
  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.usersData.asObservable();
  }

  /*
      Called when data table gets destroyed.
      It avoids memory leaks when the observables get completed
    */
  disconnect(collectionViewer: CollectionViewer): void {
    this.usersData.complete();
    this.loadingSubject.complete();
  }

  /*
    It Emmits a usersData whenever the user triggers a paginator event (empty in case of error).
    Show loading spinner before it do a request, when the request is completed the loading spinner gets hidden
  */
  fetchUsers(filter: string, sortDirection: string, pageSize: number, pageIndex: number) {

    this.loadingSubject.next(true);
    this.userService.findUsers(filter, sortDirection, pageSize, pageIndex).pipe(

      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))

    ).subscribe(pagination => {

      this.usersData.next(<User[]>pagination.data);
      this.pagesSubject.next(pagination.total);


    });

  }


}
