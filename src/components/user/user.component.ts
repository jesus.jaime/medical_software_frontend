import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';

import { merge } from 'rxjs';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { User } from '../../classes/user';
import { NewUserComponent } from './new-user/new-user.component';
import { UserDataSource } from './user-datasource';
import { UserService } from '../../services/user.service';
import { Role } from '../../classes/role';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchInput: FormControl;

  dataSource: UserDataSource;
  roles: Role[];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'email', 'created_at', 'updated_at', 'is_active', 'actions'];


  constructor(
    private userService: UserService,
    private roleService: RoleService,
    public matDialog: MatDialog) {
    this.searchInput = new FormControl();

  }

  /* Initialize datasource and load the first page */
  ngOnInit(): void {
    this.dataSource = new UserDataSource(this.userService);
    this.dataSource.fetchUsers(
      '' /* filter */,
      'asc'/* sort  */,
      10 /* page size */,
      0/* page index */);
    this.paginator._intl.itemsPerPageLabel = 'Filas por página';

    this.roleService.findAvailableRoles().subscribe(roles => {
      this.roles = roles;
    });


  }

  ngAfterViewInit(): void {

    // server-side search
    this.searchInput.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.dataSource.fetchUsers(
          this.searchInput.value,
          this.sort.direction,
          this.paginator.pageSize,
          this.paginator.pageIndex);
      })
    ).subscribe();


    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() =>
          this.dataSource.fetchUsers(
            this.searchInput.value,
            this.sort.direction,
            this.paginator.pageSize,
            this.paginator.pageIndex)
        )
      ).subscribe();

  }


  /* Open a dialog to create or update a user via POST */
  openDialog(user?: User): void {

    const dialogRef = this.matDialog.open(NewUserComponent, {
      width: '90%',
      height: '100%',
      panelClass: 'custom-dialog',
      data: {
        user: user,
        roles: this.roles

      }
    });

    dialogRef.afterClosed().subscribe(result => {

      /*POST A NEW USER TYPE */
      if (result && result.id == null) {
        this.userService.saveUser(result.user)
          .subscribe(() => {
            this.dataSource.fetchUsers(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size */,
              0 /* page index */
            );
          });
      }

      /*UPDATE EVENT STATUS TYPE */
      if (result && result.id != null) {
        this.userService.updateUser(result.id, result.user)
          .subscribe(() => {
            this.dataSource.fetchUsers(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size */,
              0 /* page index */
            );
          });
      }

    });

  }

  openCredentialsDialog(user: User): void {

  }

}
