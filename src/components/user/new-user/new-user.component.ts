import { Component, OnInit, ViewChild, Inject, AfterViewInit } from '@angular/core';
import { MatStepper, MatDialogRef, MAT_DIALOG_DATA, MatSelectionList } from '@angular/material';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

import { UserService } from '../../../services/user.service';
import { DeviceService } from '../../../services/device.service';
import { User } from '../../../classes/user';
import { Role } from '../../../classes/role';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit, AfterViewInit {

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('rolelist') roleList: MatSelectionList;


  formType: string;
  userFormGroup: FormGroup;
  width = 0;
  roles: Role[];


  constructor(
    public dialogRef: MatDialogRef<NewUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { user: User, roles: Role[] },
    private _formBuilder: FormBuilder,
    public deviceService: DeviceService,
    private userService: UserService
  ) {

  }



  ngOnInit(): void {

    this.formType = this.data.user == null ? 'create' : 'edit';
    this.width = window.innerWidth;
    this.setDialogSize();

    //Get available user roles
    this.roles = this.data.roles;
    this.roles.map((role) => { role.is_active = 0; });

    // Form initialiazing
    if (this.formType === 'edit') {
      this.initEditUserForm();
    } else {
      this.initNewUserForm();
    }



  }

  /*Subscribe to role selection event an refill user roles array form */
  ngAfterViewInit(): void {

    this.roleList.selectionChange.subscribe((list) => {

      const selectedRoles = list.source.selectedOptions.selected.map(role => role.value);
      const roleListArray = <FormArray>this.userFormGroup.get('user.roles');
      roleListArray.controls = [];
      roleListArray.setValue([]);


      for (const selectedRole of selectedRoles) {
        roleListArray.push(this._formBuilder.control(selectedRole));
      }


    });
  }

  /* Set dialog size according to the window size */
  setDialogSize(): void {
    if (this.width <= 767.98) {
      this.dialogRef.updateSize('90%', '90%');
    }

    if (this.width > 767.98 && this.width < 992) {
      this.dialogRef.updateSize('90%', '70%');

    }
    if (this.width > 992) {
      this.dialogRef.updateSize('40%', '80%');
    }

  }

  initNewUserForm(): void {
    this.userFormGroup = this._formBuilder.group({
      'person': this._formBuilder.group({
        'name': [null, Validators.required],
        'first_name': [null, Validators.required],
        'last_name': [null, Validators.required],
      }),
      'user': this._formBuilder.group({
        'email': [null, Validators.compose([
          Validators.required,
          Validators.email
        ]),
          this.isEmailAvailable.bind(this)],
        'password': [null, Validators.required],
        'roles': this._formBuilder.array([], Validators.required)
      }),

      'person_contact': this._formBuilder.array([], Validators.required)
    });
  }

  initEditUserForm(): void {

    this.userFormGroup = this._formBuilder.group({
      'person': this._formBuilder.group({
        'name': [null, Validators.required],
        'first_name': [null, Validators.required],
        'last_name': [null, Validators.required],
      }),
      'user': this._formBuilder.group({
        'roles': this._formBuilder.array([], Validators.required)
      }),

      'person_contact': this._formBuilder.array([], Validators.required)
    });

    // Set contacts rows on step 3
    const personContacts = <FormArray>this.userFormGroup.get('person_contact');
    for (const contact of this.data.user.person.contacts) {
      personContacts.push(this._formBuilder.group({
        'type': contact.type,
        'contact': contact.contact
      }));
    }

    // Set  user selected roles on step 2
    const userRoles = <FormArray>this.userFormGroup.get('user.roles');
    for (const userRole of this.data.user.roles) {
      userRoles.push(this._formBuilder.control(userRole.id));
      const selectedRole = this.roles.find(role => role.id === userRole.id);
      selectedRole.is_active = 1;
    }


    this.userFormGroup.setValue({
      'person': {
        'name': this.data.user.person.name,
        'first_name': this.data.user.person.first_name,
        'last_name': this.data.user.person.last_name,
      },
      'user': {
        'roles': userRoles.value,
      },
      'person_contact': personContacts.value
    });
  }


  addContact() {
    const contactArray = <FormArray>this.userFormGroup.get('person_contact');
    contactArray.push(this._formBuilder.group({
      'type': [null, Validators.required],
      'contact': [null, Validators.required]
    }));
  }

  removeContact(index: number) {
    const contactArray = <FormArray>this.userFormGroup.get('person_contact');
    contactArray.removeAt(index);
  }

  /*VAlidate if an email is available for a new user with a http get request*/
  isEmailAvailable(control: AbstractControl): Promise<any> | Observable<any> {

    const promise = new Promise<any>(
      (resolve, reject) => {
        this.userService.isEmailAvailable(control.value).subscribe(
          (data) => {

            if (!data.message) {
              resolve({ duplicated: true });
            }


            resolve(null);

          }, (error) => {
            resolve(null);
          }
        );
      }
    );

    return promise;

  }

  /*Close the dialog and send the form data to the user component, then it will do a http request to save data */
  onSubmit() {
    this.dialogRef.close({
      id: this.formType === 'edit' ? this.data.user.id : null,
      user: <User>this.userFormGroup.value
    });
  }



}
