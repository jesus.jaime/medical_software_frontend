import { Component, OnInit, ViewChild, Inject, AfterViewInit } from '@angular/core';
import { MatStepper, MatDialogRef, MAT_DIALOG_DATA, MatSelectionList } from '@angular/material';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { Observable, fromEvent, Subscription } from 'rxjs';

import { RoleService } from '../../../services/role.service';
import { DeviceService } from '../../../services/device.service';
import { Role } from '../../../classes/role';
import { RoleComponent } from '../role.component';
import { DialogSizes } from 'src/classes/dialog-sizes';

@Component({
  selector: 'app-new-role',
  templateUrl: './new-role.component.html',
  styleUrls: ['./new-role.component.scss']
})
export class NewRoleComponent implements OnInit {


  @ViewChild('stepper') stepper: MatStepper;


  formType: string;
  roleForm: FormGroup;
  dialogSizes: DialogSizes = {
    'sm': {
      'innerWidth': '90%',
      'innerHeight': '90%'
    },
    'md': {
      'innerWidth': '90%',
      'innerHeight': '70%'
    },
    'lg': {
      'innerWidth': '60%',
      'innerHeight': '80%'
    }

  };
  windowResizeSub: Subscription;



  constructor(
    public dialogRef: MatDialogRef<NewRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Role,
    private _formBuilder: FormBuilder,
    public deviceService: DeviceService,
    private roleService: RoleService
  ) {

    this.initForm();
  }


  //Set form type create or update and subscribe to window resize event to update dialog size
  ngOnInit(): void {

    this.formType = this.data.id == null ? 'create' : 'edit';

    this.deviceService.updateDialogSize(window.innerWidth, this.dialogSizes, this.dialogRef);

    this.windowResizeSub = fromEvent(window, 'resize').subscribe((event: any) => {
      this.deviceService.updateDialogSize(event.target.innerWidth, this.dialogSizes, this.dialogRef);
    });

  }

  ngOnDestroy(): void {
    this.windowResizeSub.unsubscribe();
  }

  initForm(): void {
    this.roleForm = this._formBuilder.group({
      'general': this._formBuilder.group({
        'code': [null, Validators.required, this.isRoleAvailable.bind(this)],
        'description': [null, Validators.required],
      }),
      'office_permissions': this._formBuilder.group({
        'can_create': [null, Validators.required],
        'can_update': [null, Validators.required],
        'can_deactivate': [null, Validators.required],
      }),
      'pacient_permissions': this._formBuilder.group({
        'can_create': [null, Validators.required],
        'can_update': [null, Validators.required],
        'can_deactivate': [null, Validators.required],
        'can_see_reports': [null, Validators.required],
        'can_see_schedule': [null, Validators.required],
        'is_pacient_owner': [null, Validators.required],
      }),
      'user_permissions': this._formBuilder.group({
        'can_create': [null, Validators.required],
        'can_update': [null, Validators.required],
        'can_deactivate': [null, Validators.required]
      }),
    });
  }






  /*VAlidate if a role code is available for a new user role with a http get request*/
  isRoleAvailable(control: AbstractControl): Promise<any> | Observable<any> {

    const promise = new Promise<any>(
      (resolve, reject) => {
        this.roleService.isCodeAvailable(control.value).subscribe(
          (data) => {

            if (!data.message) {
              resolve({ duplicated: true });
            }


            resolve(null);

          }, (error) => {
            resolve(null);
          }
        );
      }
    );

    return promise;

  }

  /*Close the dialog and send the form data to the role component, then it will do a http request to save data */
  onSubmit() {
    this.dialogRef.close({
      id: this.formType === 'edit' ? this.data.id : null,
      role: <Role>this.roleForm.value
    });
  }


}
