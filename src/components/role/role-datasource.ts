import { DataSource, CollectionViewer } from '@angular/cdk/collections';

import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';

import { RoleService } from '../../services/role.service';
import { Role } from '../../classes/role';


export class RoleDataSource extends DataSource<Role> {

  public rolesData = new BehaviorSubject<Role[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private pagesSubject = new BehaviorSubject<number>(1);

  public loading$ = this.loadingSubject.asObservable();
  public noPages$ = this.pagesSubject.asObservable();

  constructor(private roleService: RoleService) {
    super();
  }

  /*
    Called when data table bootstrap itself.
    It emits a Roles list whenever the user trigger a paginator event
    It implements a subject, with this the subscribers can get the latest emitted value
  */
  connect(collectionViewer: CollectionViewer): Observable<Role[]> {
    return this.rolesData.asObservable();
  }


  /*
    Called when data table gets destroyed.
    It avoids memory leaks when the observables get completed
  */
  disconnect(collectionViewer: CollectionViewer): void {
    this.rolesData.complete();
    this.loadingSubject.complete();
  }

  /*
    It Emmits a rolesData array whenever the user triggers a paginator event (empty in case of error).
    Show loading spinner before it do a request, when the request is completed the loading spinner gets hidden
  */
  fetchRoles(filter: string, sortDirection: string, pageSize: number, pageIndex: number) {

    this.loadingSubject.next(true);
    this.roleService.fetchByPage(filter, sortDirection, pageSize, pageIndex).pipe(

      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))

    ).subscribe(pagination => {

      this.rolesData.next(<Role[]>pagination.data);
      this.pagesSubject.next(pagination.total);

    });

  }


}
