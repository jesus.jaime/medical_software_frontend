import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';

import { merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';


import { RoleDataSource } from './role-datasource';
import { NewRoleComponent } from './new-role/new-role.component';
import { RoleService } from '../../services/role.service';
import { Role } from '../../classes/role';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchInput: FormControl;

  dataSource: RoleDataSource  ;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['code', 'description', 'created_at', 'updated_at', 'is_active', 'actions'];


  constructor(
    private roleService: RoleService,
    public matDialog: MatDialog) {
    this.searchInput = new FormControl();

  }

  /* Initialize datasource and load the first page */
  ngOnInit() {
    this.dataSource = new RoleDataSource(this.roleService);
    this.dataSource.fetchRoles(
      '' /* filter */,
      'asc' /* sort direction */,
      10 /* page size*/,
      0 /* page index*/);

    this.paginator._intl.itemsPerPageLabel = 'Filas por página';

  }


  ngAfterViewInit() {

    // server-side search
    this.searchInput.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.dataSource.fetchRoles(
          this.searchInput.value,
          this.sort.direction,
          this.paginator.pageSize,
          this.paginator.pageIndex);
      })
    ).subscribe();


    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() =>
          this.dataSource.fetchRoles(
            this.searchInput.value,
            this.sort.direction,
            this.paginator.pageSize,
            this.paginator.pageIndex)
        )
      ).subscribe();

  }


  /* Open a dialog to create or update a role type via POST */
  openDialog(role?: Role): void {

    const dialogRef = this.matDialog.open(NewRoleComponent, {
      width: '30%',
      panelClass: 'custom-dialog',
      data: {
        ...role
      }
    });


    dialogRef.afterClosed().subscribe(result => {

      /*POST A NEW ROLE */
      if (result && result.id == null) {
        this.roleService.saveRole(result.role)
          .subscribe(() => {
            this.dataSource.fetchRoles(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size*/,
              0 /* page index*/);
          });
      }

      /*UPDATE EVENT TYPE */
      if (result && result.id != null) {
        this.roleService.updateRole(result.id, result.role)
          .subscribe(() => {
            this.dataSource.fetchRoles(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size*/,
              0 /* page index*/);
          });
      }
    });

  }

}
