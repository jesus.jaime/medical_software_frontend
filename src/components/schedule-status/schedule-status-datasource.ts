import { DataSource, CollectionViewer } from '@angular/cdk/collections';

import { Observable, of as observableOf, merge, BehaviorSubject, of } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';

import { ScheduleStatus } from '../../classes/schedule-status';
import { ScheduleStatusService } from '../../services/schedule-status.service';


export class ScheduleStatusDataSource extends DataSource<ScheduleStatus> {

  private eventStTypeData = new BehaviorSubject<ScheduleStatus[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private pagesSubject = new BehaviorSubject<number>(1);

  public loading$ = this.loadingSubject.asObservable();
  public noPages$ = this.pagesSubject.asObservable();

  constructor(private scheduleStService: ScheduleStatusService) {
    super();
  }

  /*
    Called when data table bootstrap itself.
    It emits a Schedule Status list whenever the user trigger a paginator event
    It implements a subject, with this the subscribers can get the latest emitted value
  */
  connect(collectionViewer: CollectionViewer): Observable<ScheduleStatus[]> {
    return this.eventStTypeData.asObservable();
  }

  /*
      Called when data table gets destroyed.
      It avoids memory leaks when the observables get completed
    */
  disconnect(collectionViewer: CollectionViewer): void {
    this.eventStTypeData.complete();
    this.loadingSubject.complete();
  }

  /*
    It Emmits a eventStTypeData whenever the user triggers a paginator event (empty in case of error).
    Show loading spinner before it do a request, when the request is completed the loading spinner gets hidden
  */
  fetchEventStatusType(filter: string, sortDirection: string, pageSize: number, pageIndex: number) {

    this.loadingSubject.next(true);
    this.scheduleStService.findStatuses(filter, sortDirection, pageSize, pageIndex).pipe(

      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))

    ).subscribe(pagination => {

      this.eventStTypeData.next(<ScheduleStatus[]>pagination.data);
      this.pagesSubject.next(pagination.total);


    });

  }


}
