import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ScheduleStatusComponent } from '../schedule-status.component';
import { ScheduleStatus } from '../../../classes/schedule-status';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-schedule-status',
  templateUrl: './new-schedule-status.component.html',
  styleUrls: ['./new-schedule-status.component.scss']
})
export class NewScheduleStatusComponent implements OnInit {

  dialogTitle: String = '';
  statusColor: String = '';
  scheduleStForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ScheduleStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ScheduleStatus,
    private formBuilder: FormBuilder
  ) {

    this.statusColor = '#eaeaea';
    this.initForm();


  }

  /* It receives a schedule status type object from an opened Dialog (Create or update), set the values to the form */
  ngOnInit(): void {
    this.dialogTitle = !this.data.id ? 'Nuevo Status Agenda' : 'Editar Status Agenda';

    if (this.data.id) {
      this.statusColor = this.data.color;
      this.scheduleStForm.get('name').setValue(this.data.name);
      this.scheduleStForm.get('color').setValue(this.data.color);
    }
  }

  initForm(): void {

    this.scheduleStForm = this.formBuilder.group({
      'name': [
        null,
        Validators.required],
      'color': [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/^#[0-9A-F]{6}$/i)
        ])
      ]
    });


  }

  /* Set a value to the color form control */
  onSelectColor(color: string) {
    this.scheduleStForm.get('color').setValue(this.statusColor);
  }

  /* Return a scheduleStatusType object to Schedule Status Component to do a http request */
  onSubmit() {

    this.dialogRef.close({
      id: this.data == null ? null : this.data.id,
      scheduleStatus: this.scheduleStForm.value
    });

  }
}
