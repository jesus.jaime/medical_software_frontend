import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';

import { merge } from 'rxjs';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { ScheduleStatus } from '../../classes/schedule-status';
import { NewScheduleStatusComponent } from './new-schedule-status/new-schedule-status.component';
import { ScheduleStatusDataSource } from './schedule-status-datasource';
import { ScheduleStatusService } from '../../services/schedule-status.service';



@Component({
  selector: 'app-schedule-status',
  templateUrl: './schedule-status.component.html',
  styleUrls: ['./schedule-status.component.scss']
})
export class ScheduleStatusComponent implements AfterViewInit, OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchInput: FormControl;

  dataSource: ScheduleStatusDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['color', 'name', 'created_at', 'updated_at', 'is_active', 'actions'];


  constructor(
    private scheduleStService: ScheduleStatusService,
    public matDialog: MatDialog) {
    this.searchInput = new FormControl();

  }

  /* Initialize datasource and load the first page */
  ngOnInit(): void {
    this.dataSource = new ScheduleStatusDataSource(this.scheduleStService);
    this.dataSource.fetchEventStatusType(
      '' /* filter */,
      'asc'/* sort  */,
      10 /* page size */,
      0/* page index */);

    this.paginator._intl.itemsPerPageLabel = 'Filas por página';

  }

  ngAfterViewInit(): void {

    // server-side search
    this.searchInput.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.dataSource.fetchEventStatusType(
          this.searchInput.value,
          this.sort.direction,
          this.paginator.pageSize,
          this.paginator.pageIndex);
      })
    ).subscribe();


    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() =>
          this.dataSource.fetchEventStatusType(
            this.searchInput.value,
            this.sort.direction,
            this.paginator.pageSize,
            this.paginator.pageIndex)
        )
      ).subscribe();


  }


  /* Open a dialog to create or update a calendar event status type via POST */
  openDialog(scheduleStatus?: ScheduleStatus): void {

    const dialogRef = this.matDialog.open(NewScheduleStatusComponent, {
      width: '30%',
      data: {
        ...scheduleStatus
      }
    });

    dialogRef.afterClosed().subscribe(result => {

      /*POST A NEW EVENT STATUS TYPE */
      if (result && result.id == null) {
        this.scheduleStService.saveStatus(result.scheduleStatus)
          .subscribe(() => {
            this.dataSource.fetchEventStatusType(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size */,
              0 /* page index */
            );
          });
      }

      /*UPDATE EVENT STATUS TYPE */
      if (result && result.id != null) {
        this.scheduleStService.updateStatus(result.id, result.scheduleStatus)
          .subscribe(() => {
            this.dataSource.fetchEventStatusType(
              '' /* filter */,
              'asc' /* sort direction */,
              10 /* page size */,
              0 /* page index */
            );
          });
      }

    });

  }

}
