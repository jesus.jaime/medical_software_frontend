import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RequestMessages } from '../classes/request-messages';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  constructor(
    public snackBar: MatSnackBar
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let headers: HttpHeaders;

    // Login and default headers
    if (request.url.includes('login')) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      });

    }


    const clonedRequest = request.clone({
      url: environment.projectUrl + request.url,
      headers: headers
    });


    return next.handle(clonedRequest).pipe(
      tap((event: HttpEvent<any>) => {

        const isMessageable = RequestMessages.REQUESTS.find(x => clonedRequest.url.includes(x));

        if (event instanceof HttpResponse && isMessageable) {


          this.snackBar.open(event.body.message, 'OK', {
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['snackbar-custom', 'snackbar-success'],

          });

        }


      }, (error: HttpErrorResponse) => {


        let msg = '';
        const errorObj = error.error.errors;

        for (const key in errorObj) {
          if (Array.isArray(errorObj[key])) {
            msg += errorObj[key][0] + '\n';
          } else {
            msg += errorObj[key] + '\n';
          }

        }

        this.snackBar.open(msg, 'OK', {
          duration: 20000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
          panelClass: ['snackbar-custom', 'snackbar-danger'],

        });

      })
    );

    /*  return next.handle(clonedRequest).tap((event: HttpEvent<any>) => {

       if (event instanceof HttpResponse) {

         toastMessage.setDuration(3000);
         if (event.body.message != undefined && ToasterRequests.REQUESTS.find(x => clonedRequest.url.includes(x))) {
           this.commonService.presentToaster(toastMessage, 'toaster-success', event.body.message);
         }

       }
     },
       (error: any) => {

         if (error instanceof HttpErrorResponse) {
           toastMessage.setDuration(10000);
           if (error.status != 401) {
             let msg = '', errorObj = JSON.parse(error.error);

             for (let key in errorObj) {
               if (Array.isArray(errorObj[key])) {
                 msg += errorObj[key][0] + '\n';
               } else {
                 msg += errorObj[key] + '\n';
               }

             }
             this.commonService.presentToaster(toastMessage, 'toaster-danger-breakline', msg);
           }

           if (error.status === 401) {
             this.events.publish('user:logout');
           }
         }
       });
   } */
  }
}
