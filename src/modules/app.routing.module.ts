import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleStatusComponent } from '../components/schedule-status/schedule-status.component';
import { ScheduleEventComponent } from '../components/schedule-event/schedule-event.component';
import { UserComponent } from '../components/user/user.component';
import { RoleComponent } from '../components/role/role.component';

const appRoutes: Routes = [
  { path: 'scheduleStatuses', component: ScheduleStatusComponent },
  { path: 'scheduleEvents', component: ScheduleEventComponent },
  { path: 'users', component: UserComponent },
  { path: 'roles', component: RoleComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})


export class AppRoutingModule {

}
