import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PaginationModule } from '../material/pagination.module';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';


import { ScheduleStatusComponent } from '../../components/schedule-status/schedule-status.component';
import { NewScheduleStatusComponent } from '../../components/schedule-status/new-schedule-status/new-schedule-status.component';

import { ScheduleStatusService } from '../../services/schedule-status.service';


@NgModule({
  declarations: [
    ScheduleStatusComponent,
    NewScheduleStatusComponent

  ],
  entryComponents: [
    NewScheduleStatusComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    MatChipsModule,
    MatGridListModule,
    MatDividerModule,
    MatDialogModule,
    ColorPickerModule

  ],
  providers: [
    ScheduleStatusService
  ]

})
export class ScheduleStatusModule { }
