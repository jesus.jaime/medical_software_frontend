import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaginationModule } from '../material/pagination.module';
import { ColorPickerModule } from 'ngx-color-picker';

import { ScheduleEventComponent } from '../../components/schedule-event/schedule-event.component';
import { NewScheduleEventComponent } from '../../components/schedule-event/new-schedule-event/new-schedule-event.component';
import { ConfirmDialogComponent } from 'src/components/_partials/confirm-dialog/confirm-dialog.component';


import { ScheduleEventService } from '../../services/schedule-event.service';


@NgModule({
  declarations: [
    ScheduleEventComponent,
    NewScheduleEventComponent,
    ConfirmDialogComponent

  ],
  entryComponents: [
    NewScheduleEventComponent,
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    PaginationModule,
    ColorPickerModule

  ],
  providers: [
    ScheduleEventService
  ]

})
export class ScheduleEventModule { }
