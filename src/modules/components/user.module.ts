import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaginationModule } from '../material/pagination.module';
import { MatStepperModule } from '@angular/material/stepper';

import { NewUserComponent } from '../../components/user/new-user/new-user.component';
import { UserComponent } from '../../components/user/user.component';
import { UserService } from '../../services/user.service';
import { RoleService } from '../../services/role.service';
import { MatListModule, MatSelectModule } from '@angular/material';





@NgModule({
  declarations: [
    UserComponent,
    NewUserComponent

  ],
  entryComponents: [
    NewUserComponent
  ],
  imports: [
    CommonModule,
    PaginationModule,
    MatListModule,
    MatSelectModule,
    MatStepperModule,

  ],
  providers: [
    UserService,
    RoleService
  ]

})
export class UserModule { }
