import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PaginationModule } from '../material/pagination.module';
import { MatChipsModule } from '@angular/material/chips';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule, MatStepperModule } from '@angular/material';


import { RoleComponent } from '../../components/role/role.component';
import { NewRoleComponent } from '../../components/role/new-role/new-role.component';
import { RoleService } from '../../services/role.service';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
    declarations: [
        RoleComponent,
        NewRoleComponent
    ],
    entryComponents: [
        NewRoleComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        MatChipsModule,
        MatGridListModule,
        MatDividerModule,
        MatDialogModule,
        MatStepperModule,
        FlexLayoutModule
    ],
    providers: [
        RoleService
    ]

})
export class RoleModule { }
