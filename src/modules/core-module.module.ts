import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule, MatCardModule } from '@angular/material';

import { AppRoutingModule } from './app.routing.module';
import { NavigationModule } from './material/navigation.module';

import { AppComponent } from '../app/app.component';
import { MainNavComponent } from '../components/_partials/main-nav/main-nav.component';







@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NavigationModule,
    MatSnackBarModule

  ],
  declarations: [
    AppComponent,
    MainNavComponent
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule


  ]
})
export class CoreModule { }
